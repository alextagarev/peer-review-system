export interface Team {
    id: string;
    name: string;
    createdOn: Date;
    authorId: string;
}
