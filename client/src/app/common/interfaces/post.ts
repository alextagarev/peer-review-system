import { CommentInterface } from './show-comment';

export interface Post {
  id: string;
  title: string;
  description: string;
  tags: string;
  status: string;
  createdOn: Date;

  comments?: CommentInterface[];
  postLikes: number;
  postDislikes: number;
  assigneeId: string;

}
