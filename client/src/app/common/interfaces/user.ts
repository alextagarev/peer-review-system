export interface User {
    id: string;
    name: string;
    email: string;
    friends?: User[];
    createdOn: Date;
}
