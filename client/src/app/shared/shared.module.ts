import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BreadcrumbsSearchComponent } from '../components/breadcrumbs-search/breadcrumbs-search.component';
import { CreatePostButtonComponent } from '../posts/create-post-button/create-post-button.component';
import { RouterModule } from '@angular/router';
import { SearchBoxComponent } from '../components/search-box/search-box.component';
import { CreateTeamButtonComponent } from '../teams/create-team-button/create-team-button.component';

@NgModule({
  declarations: [
    BreadcrumbsSearchComponent,
    CreatePostButtonComponent,
    CreateTeamButtonComponent,
    SearchBoxComponent,
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
  ],
  exports: [
    CommonModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    BreadcrumbsSearchComponent,
    CreatePostButtonComponent,
    SearchBoxComponent,
  ],
})
export class SharedModule { }
