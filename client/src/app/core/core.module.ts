import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StorageService } from './services/storage.service';
import { NotificatorService } from './services/notificator.service';
import { AuthService } from './services/auth.service';
import { ToastrModule } from 'ngx-toastr';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ToastrModule.forRoot({
      timeOut: 5000,
      extendedTimeOut: 1000,
      preventDuplicates: true,
      easing: 'ease-in',
      easeTime: 1000,
      newestOnTop: false,
    }),
    HttpClientModule,
  ],
  providers: [
    StorageService,
    NotificatorService,
    AuthService,
  ],
})
export class CoreModule { }
