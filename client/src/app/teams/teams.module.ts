import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { PostsRoutingModule } from '../posts/posts-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UsersComponent } from './users.component';
import { UsersRoutingModule } from './users-routing.module';
import { UsersViewComponent } from './user-view/users-view.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { PostsModule } from '../posts/posts.module';
import { CreateTeamComponent } from './create-team/create-team.component';

@NgModule({
  declarations: [
    UsersComponent,
    UsersViewComponent,
    UserProfileComponent,
    CreateTeamComponent,
  ],
  imports: [
    SharedModule,
    RouterModule,
    UsersRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    PostsModule,
  ],
  exports: [
     UsersComponent
  ],
})
export class TeamsModule { }
