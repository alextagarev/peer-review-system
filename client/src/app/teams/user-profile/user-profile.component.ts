import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../../common/interfaces/user';

import { Subscription } from 'rxjs';
import { AuthService } from '../../../app/core/services/auth.service';
import { NotificatorService } from '../../../app/core/services/notificator.service';
import { PostsDataService } from '../../../app/posts/services/posts-data.service';
import { PostsCommentsBaseComponent } from '../../../app/posts/posts-comments--base.component';
import { TeamsDataService } from '../services/users-data.service';
import { post } from 'selenium-webdriver/http';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent extends PostsCommentsBaseComponent implements OnInit, OnDestroy {
  public friends: User[] = [];
  public loggedInUserSubscription: Subscription;
  public loggedInUser: string;
  public loggedInUserId: string;
  public isFried: boolean = false;
  public showAddRemoveFriendButtons: boolean = true;
  public mememberName: string;

  public currentJustify = 'justified';

  constructor(
    notificator: NotificatorService,
    postsDataService: PostsDataService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly usersDataService: TeamsDataService,
    private readonly authService: AuthService,
    private readonly router: Router,
  ) {
    super(postsDataService, notificator);
  }

  ngOnInit(): void {
    this.activatedRoute
      .data
      .subscribe(
        (data) => {
          this.user = data.user;

          this.usersDataService
            .postsOfUser(data.user.team.id)
            .subscribe(
              (posts) => {
                this.data = posts;
       
                this.mememberName = data.user.teamMembers[0].name;
            }
          );

          // this.usersDataService.commentsOfUser(this.user.id)
          //   .subscribe(
          //     (comments) => {
          //       this.comments = comments;
          //     }
          //   );

          // this.usersDataService.userFriends(this.user.id)
          //   .subscribe(
          //     (friends) => {
          //       this.friends = friends; // It can be done with or without another array of type User[]
          //       this.user.friends = friends;
          //     }
          //   );

          this.loggedInUserSubscription = this.authService.user$.subscribe(
            (user) => {
              this.loggedInUser = user;
              // request to find loggedInUser
              this.usersDataService.allUsers().subscribe(
                (users) => {
          
                  users.forEach( (user) => {
                    if(user.name === this.loggedInUser) {
                      this.loggedInUserId = user.id;
                      if(this.loggedInUserId === this.user.id){
                        this.showAddRemoveFriendButtons = false;
                      }
                      this.usersDataService
                        .userFriends(this.loggedInUserId)
                        .subscribe((people: User[])=>{
                          const foundFriend = people.find(
                            (currentUser)=>currentUser.id === this.user.id);
                          if(foundFriend){
                            this.isFried = true;
                          }
                        });
                      return;
                    }
                  });
                }
              );
            }
          );
        }
    );
  }

  ngOnDestroy() {
    this.loggedInUserSubscription.unsubscribe();
  }

  public addFriend(): void {
    this.router.navigate([`/users/m/${this.user.team.id}`]);
  }

  public removeFriend(): void {

    this.usersDataService
      .removeFriend(this.user.id)
      .subscribe((noLongerFriend)=>this.isFried=false);
  }

}

