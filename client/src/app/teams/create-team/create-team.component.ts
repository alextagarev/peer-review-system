import { Post } from './../../common/interfaces/post';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotificatorService } from '../../../app/core/services/notificator.service';
import { TeamsDataService } from '../services/users-data.service';
import { Team } from 'src/app/common/interfaces/team';

@Component({
  selector: 'app-create-team',
  templateUrl: './create-team.component.html',
  styleUrls: ['./create-team.component.css']
})
export class CreateTeamComponent implements OnInit {
  public createPostForm: FormGroup;

  public options: Object = {
    placeholderText: 'Write your post here...',
    charCounterCount: true,
    heightMin: 170,
    heightMax: 300,
    attribution: false,
    toolbarBottom: true
  };

  constructor(
    private readonly usersDataService: TeamsDataService,
    private readonly formBuilder: FormBuilder,
    private readonly router: Router,
    private readonly notificator: NotificatorService
  ) { }

  ngOnInit() {
    this.createPostForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(4)]],

    });
  }

  public createPost(): void {

    this.usersDataService.createPost(this.createPostForm.value).subscribe(
      (post: Team) => {
    
        
        this.router.navigate([`/teams/${post.id}`]);
        this.notificator.success('Team successfully created !');
        this.notificator.success('Add new Members !');
        
      },
      (error) => {
        this.notificator.error('Team creation unsuccessful !');
      }
    );
  }

}
