import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from '../../../app/common/interfaces/user';

@Component({
    selector: 'app-users-view',
    templateUrl: './users-view.component.html',
    styleUrls: ['./users-view.component.css']
  })
  export class UsersViewComponent {
  
    @Input() public user: User;
    constructor(
    ) {
        
     }
  }