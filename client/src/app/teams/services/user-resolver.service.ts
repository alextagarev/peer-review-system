
import { of } from 'rxjs';
import { User } from '../../common/interfaces/user';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { NotificatorService } from '../../../app/core/services/notificator.service';
import { TeamsDataService } from './users-data.service';
import { catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class TeamsResolverService implements Resolve<User[] | {users: User[]}> {

  constructor(
    private readonly usersDataService: TeamsDataService,
    private readonly notificator: NotificatorService,
  ) { }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ) {  



    return this.usersDataService.allUsers()
        .pipe(catchError(
          (res) => {
            this.notificator.error(res.error.error);

            return of({users: null});
          }
        ));
  }
}
