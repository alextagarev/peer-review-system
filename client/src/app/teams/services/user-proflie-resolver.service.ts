
import { of } from 'rxjs';
import { User } from '../../common/interfaces/user';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { NotificatorService } from '../../../app/core/services/notificator.service';
import { TeamsDataService } from './users-data.service';
import { catchError } from 'rxjs/operators';
import { Team } from 'src/app/common/interfaces/team';
@Injectable({
  providedIn: 'root'
})
export class UserProfileResolverService implements Resolve<Team | {user: Team}> {
  private userId: string;
  constructor(
    private readonly usersDataService: TeamsDataService,
    private readonly notificator: NotificatorService,
  ) { }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ) {


    this.userId = route.params['id'];

    return this.usersDataService.singleUser(this.userId)
        .pipe(catchError(
          (res) => {
            this.notificator.error(res.error.error);

            return of({user: null});
          }
        ));
  }
}
