import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from '../../../app/common/interfaces/user';
import { UsersDataService } from '../services/users-data.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';

@Component({
    selector: 'app-users-view',
    templateUrl: './users-view.component.html',
    styleUrls: ['./users-view.component.css']
  })
  export class UsersViewComponent {
  
    @Input() public user: User;
    constructor(
      private readonly usersDataService: UsersDataService,
      private readonly notificator: NotificatorService,
    ) {

     }

     public addFriend(userId) {
      const teamId = window.location.href.split('/')[5];

       this.usersDataService
         .addFriend(teamId, userId)
         .subscribe((friend)=>(friend));
         this.notificator.success('Member added to Team !');

     }
  }