import { PostsDataService } from './posts-data.service';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, ActivatedRoute, Params } from '@angular/router';
import { Post } from '../../../app/common/interfaces/post';
import { NotificatorService } from '../../../app/core/services/notificator.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostsResolverService implements Resolve<Post[] | {posts: Post[]}> {
  public page: number;

  constructor(
    private readonly postsDataService: PostsDataService,
    private readonly notificator: NotificatorService,
  ) { }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ) {
    if (route.queryParams.page) {
      this.page = route.queryParams.page;
    } else {
      this.page = 1;
    }

    return this.postsDataService.allPosts(this.page)
      .pipe(catchError(
        res => {
          this.notificator.error(res.error.error);
          return of({posts: []});
        }
      ));
  }
}
