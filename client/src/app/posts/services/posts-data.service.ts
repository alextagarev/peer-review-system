import { CreatePost } from './../../common/interfaces/create-post';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from '../../../app/common/interfaces/post';
import { CreateComment } from '../../common/interfaces/create-comment';
import { CommentInterface } from './../../common/interfaces/show-comment';
import { Vote } from 'src/app/common/interfaces/vote';

@Injectable({
  providedIn: 'root'
})
export class PostsDataService {

  constructor(
    private readonly http: HttpClient,
  ) { }

  public allPosts(page = 1, user = ''): Observable<Post[]> {
    return this.http.get<Post[]>(`http://localhost:3000/users/reviews/all/`);
  }

  public allPostsCount(): Observable<number> {
    return this.http.get<number>(`http://localhost:3000/users/reviews/?count=all`);
  }

  public searchPosts(page = 1, search: string): Observable<Post[]> {
    return this.http.get<Post[]>(`http://localhost:3000/api/posts?page=${page}&search=${search}`);
  }

  public singlePost(postid: string): Observable<Post> {
    return this.http.get<Post>(`http://localhost:3000/users/reviews/${postid}`);
  }

  public createPost(post: CreatePost): Observable<Post> {
    return this.http.post<Post>('http://localhost:3000/users/reviews', post);
  }

  public updatePost(post: Post): Observable<Post> {
    return this.http.put<Post>(`http://localhost:3000/api/posts/${post.id}`, post);
  }

  public deletePost(postid: string): Observable<Post> {
    return this.http.delete<Post>(`http://localhost:3000/api/posts/${postid}`);
  }

  public createCommentOfPost(postid: string, body: any): Observable<CreateComment> {
    return this.http.post<CreateComment>(`http://localhost:3000/users/${postid}`, body);
  }

  public createComment(postid: string, comment: CreateComment ): Observable<CommentInterface> {
    return this.http.post<CommentInterface>(`http://localhost:3000/users/posts/${postid}/comments`, comment);
  }

  public acceptRevReq(voteId: any): Observable<Vote> {
    return this.http.put<Vote>(`http://localhost:3000/users/workItems/accepted/${voteId}`, true);
  }

  public rejectRevReq(voteId: any): Observable<Vote> {
    return this.http.put<Vote>(`http://localhost:3000/users/workItems/rejected/${voteId}`, true);
  }

  public updateComment(commentid: string, comment: CommentInterface): Observable<CommentInterface> {
    return this.http.put<CommentInterface>(`http://localhost:3000/api/posts/comments/${commentid}`, comment);
  }

  public deleteComment(commentid: string): Observable<CommentInterface> {
    return this.http.delete<CommentInterface>(`http://localhost:3000/api/posts/comments/${commentid}`);
  }

  public updatePostVote(postid: string, vote: boolean): Observable<Post> {
    const newVote = {
      state: vote,
    };

    return this.http.put<Post>(`http://localhost:3000/api/posts/${postid}/votes`, newVote);
  }

  public updateCommentVote(commentid: string, vote: boolean): Observable<CommentInterface> {
    const newVote = {
      state: vote,
    };

    return this.http.put<CommentInterface>(`http://localhost:3000/api/posts/comments/${commentid}/votes`, newVote);
  }
}
