import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PostsComponent } from './posts.component';
import { PostDetailsComponent } from './post-details/post-details.component';
import { PostsResolverService } from './services/posts-resolver.service';
import { PostDetailsResolverService } from './services/post-details-resolver.service';
import { CreatePostComponent } from './create-post/create-post.component';
import { PostCountResolverService } from './services/post-count-resolver.service';

const routes: Routes = [
  { path: '', component: PostsComponent, resolve: {posts: PostsResolverService, count: PostCountResolverService} },
  { path: 'create/:id', component: CreatePostComponent },
  { path: ':id', component: PostDetailsComponent, resolve: {post: PostDetailsResolverService} },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PostsRoutingModule {}
