import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Post } from './../../common/interfaces/post';

@Component({
  selector: 'app-post-view',
  templateUrl: './post-view.component.html',
  styleUrls: ['./post-view.component.css'],
})
export class PostViewComponent implements OnInit {
  @Input() public showContent: boolean;
  @Input() public post: any;
  @Input() public loggedInUser: string;

  @Output() public liked = new EventEmitter<string>();
  @Output() public disliked = new EventEmitter<string>();
  @Output() public updatedItem = new EventEmitter<Post>();
  @Output() public deletedItem = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  
    
  }

  public itemLike(): void {
    this.liked.emit(this.post.id);
  }

  public itemDislike(): void {
    this.disliked.emit(this.post.id);
  }

  public updateItem(post: Post): void {
    this.post = post;
    this.updatedItem.emit(this.post);
  }

  public deleteItem(): void {
    this.deletedItem.emit(this.post.id);
  }

}
