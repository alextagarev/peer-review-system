import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-update-comment',
  templateUrl: './update-comment.component.html',
  styleUrls: ['./update-comment.component.css']
})
export class UpdateCommentComponent implements OnInit {

  @Output() public updateComment = new EventEmitter<string>();
  @Input() public textNew: string;
  public editable = false;


  constructor() { }

  ngOnInit() {
  }

  public updateCommentOfYours(value: string) : void {

    this.updateComment.emit(value);
    this.editable = false;
  }

  public enableEdit(): void {
      this.editable = true;
  }



}
