import { Post } from './../../common/interfaces/post';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { PostsDataService } from './../services/posts-data.service';
import { Router } from '@angular/router';
import { NotificatorService } from '../../../app/core/services/notificator.service';
import { UsersDataService } from 'src/app/users/services/users-data.service';
import { AngularEditorConfig } from '@kolkov/angular-editor';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css']
})
export class CreatePostComponent implements OnInit {
  public createPostForm: FormGroup;
  public members: any[];
  public reviewers = [] = [];
  public teamId = window.location.href.split('/')[5];

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    minHeight: '5rem',
    maxHeight: '15rem',
    placeholder: 'Code, image or video',
    translate: 'no',
    sanitize: false,
    toolbarPosition: 'top',
    defaultFontName: 'Arial',
  };


  constructor(
    private readonly postsDataService: PostsDataService,
    private readonly formBuilder: FormBuilder,
    private readonly router: Router,
    private readonly notificator: NotificatorService,
    private readonly usersDataService: UsersDataService,
  ) { }

  ngOnInit() {
 
    this.createPostForm = this.formBuilder.group({
      title: ['', [Validators.required, Validators.minLength(3)]],
      description: ['', [Validators.required, Validators.minLength(3)]],
      comments: ['', [Validators.required, Validators.minLength(3)]],
      reviewers: [''],
      teamId: this.teamId
    });

    const teamId = window.location.href.split('/')[5];

    this.usersDataService.getTeamMembers(teamId).subscribe((data)=> {

      const name = data.map(x => x.email);
      this.reviewers.push(name);
      
    });

  }

  public createPost(): void {

    this.postsDataService.createPost(this.createPostForm.value).subscribe(
      (post: Post) => {
        this.router.navigate(['/posts']);
        this.notificator.success('Review Request created !');
      },
      (error) => {
        this.notificator.error('Review Request unsuccessful !');
      }
    );
  }

}
