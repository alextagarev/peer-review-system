import { NotificatorService } from '../core/services/notificator.service';
import { PostsDataService } from './services/posts-data.service';
import { Post } from '../common/interfaces/post';
import { User } from '../common/interfaces/user';
import { CommentInterface } from '../common/interfaces/show-comment';

export class PostsCommentsBaseComponent {
  public user: any;
  public data: any[] = [];
  public comments: CommentInterface[] = [];

  constructor(
      protected readonly postsDataService: PostsDataService,
      protected readonly notificator: NotificatorService,
  ) { }

  public itemLike(postId: string): void { 
      this.postsDataService.updatePostVote(postId, true).subscribe(
        (post: Post) => {
          const updatedPost = this.data.find((item: Post) => item.id === post.id);
          const updatePostIndex = this.data.indexOf(updatedPost);

          this.data[updatePostIndex].postLikes = post.postLikes;
          this.data[updatePostIndex].postDislikes = post.postDislikes;

          this.notificator.success('You liked the post successfully!');
        },
        (error) => {
          if (error.error.message === 'User has already liked this post.') {
            this.notificator.error('You have already liked this post.');
          }
        }
      );
    }

  public itemDislike(postId: string): void {
    this.postsDataService.updatePostVote(postId, false).subscribe(
      (post: Post) => {
        const updatedPost = this.data.find((item: Post) => item.id === post.id);
        const updatedPostIndex = this.data.indexOf(updatedPost);

        this.data[updatedPostIndex].postLikes = post.postLikes;
        this.data[updatedPostIndex].postDislikes = post.postDislikes;

        this.notificator.success('You disliked the post successfully!');
      },
      (error) => {
        if (error.error.message === 'User has already disliked this post.') {
          this.notificator.error('You have already disliked this post.');
        }
      }
    );
  }

  public updateItem(item: Post): void {
    this.postsDataService.updatePost(item).subscribe(
      (post: Post) => {
        this.notificator.success('Post updated successfully!');
      },
      (error) => {
        this.notificator.error('Post update failed!');
      }
    );
  }

  public deleteItem(postId: string): void {
    this.postsDataService.deletePost(postId).subscribe(
      (post: Post) => {
        const deletedPost = this.data.find((item: Post) => item.id === post.id);
        const deletedPostIndex = this.data.indexOf(deletedPost);

        this.data.splice(deletedPostIndex, 1);
  

        this.notificator.success('Post deteled successfully!');
      },
      (error) => {
        this.notificator.error('Post delete failed!');
      }
    );
  }

  public commentLike(commentId: string): void {
    this.postsDataService.updateCommentVote(commentId, true).subscribe(
        (comment: CommentInterface) => {
          const updatedComment = this.comments.find((item: CommentInterface) => item.id === comment.id);
          const updatedCommentIndex = this.comments.indexOf(updatedComment);

          this.comments[updatedCommentIndex].commentLikes = comment.commentLikes;
          this.comments[updatedCommentIndex].commentDislikes = comment.commentDislikes;

          this.notificator.success('You liked the comment successfully!');
        },
        (error) => {
          if (error.error.message === 'User has already liked this comment.') {
              this.notificator.error('You have already liked this comment.');
          }
        }
    );
  }

  public commentDislike(commentId: string): void {
    this.postsDataService.updateCommentVote(commentId, false).subscribe(
      (comment: CommentInterface) => {
        const updatedComment = this.comments.find((item: CommentInterface) => item.id === comment.id);
        const updatedCommentIndex = this.comments.indexOf(updatedComment);

        this.comments[updatedCommentIndex].commentLikes = comment.commentLikes;
        this.comments[updatedCommentIndex].commentDislikes = comment.commentDislikes;

        this.notificator.success('You disliked the comment successfully!');
      },
      (error) => {
        if (error.error.message === 'User has already disliked this comment.') {
          this.notificator.error('You have already disliked this comment.');
        }
      }
    );
  }

  public updateComment(item: CommentInterface): void {
    this.postsDataService.updateComment(item.id, item).subscribe(
      (comment: CommentInterface) => {
        this.notificator.success('Comment updated successfully!');
      },
      (error) => {
        this.notificator.error('Comment update failed!');
      }
    );
  }

  public deleteComment(commentId: string): void {
    this.postsDataService.deleteComment(commentId).subscribe(
      (comment: CommentInterface) => {
        const deletedComment = this.comments.find((item: CommentInterface) => item.id === comment.id);
        const deletedCommentIndex = this.comments.indexOf(deletedComment);

        this.comments.splice(deletedCommentIndex, 1);

        this.notificator.success('Comment deteled successfully!');
      },
      (error) => {
        this.notificator.error('Comment delete failed!');
      }
    );
  }
}
