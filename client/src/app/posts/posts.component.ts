import { Subscription } from 'rxjs';
import { AuthService } from './../core/services/auth.service';
import { PostsDataService } from './services/posts-data.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Post } from '../common/interfaces/post';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NotificatorService } from '../core/services/notificator.service';
import { Location } from '@angular/common';
import { PostsCommentsBaseComponent } from './posts-comments--base.component';
import { SearchService } from '../core/services/search.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent extends PostsCommentsBaseComponent implements OnInit, OnDestroy {
  public loggedInUserSubscription: Subscription;
  public searchSubscription: Subscription;
  public loggedInUser: string;

  public paginationPage = 1;
  public pageSize = 5;
  public paginationCollectionSize: number;
  public viewing = 5;
  public through = 5;
  public paginationDisabled = false;

  constructor(
    postsDataService: PostsDataService,
    notificator: NotificatorService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly location: Location,
    private readonly searchService: SearchService,
  ) {
    super(postsDataService, notificator);
  }

  ngOnInit() {
    this.activatedRoute.data.subscribe(data => this.data = data.posts);
    this.activatedRoute.data.subscribe(data => this.paginationCollectionSize = data.count);

    this.loggedInUserSubscription = this.authService.user$.subscribe(
      (user) => this.loggedInUser = user
    );

    this.activatedRoute.queryParamMap.subscribe(
      (params: Params) => {
        if (params.params.page) {
          this.paginationPage = params.params.page;
        }
      }
    );

    this.searchSubscription = this.searchService.search$.subscribe(
      (search: string) => {
        if (search === '' || search === 'clearTheSearch') {
          this.paginationDisabled = false;
          this.paginationPage = 1;
          this.postsDataService.allPosts().subscribe(
            (posts: Post[]) => {
              this.data = posts;
            }
          );
        } else if (search !== '' && search !== 'clearTheSearch') {
          this.postsDataService.searchPosts(this.paginationPage, search).subscribe(
            (posts: Post[]) => {
              this.paginationDisabled = true;
              this.data = posts;
            }
          );
        }
      }
    );
  }

  ngOnDestroy() {
    this.loggedInUserSubscription.unsubscribe();
    this.searchSubscription.unsubscribe();

    this.paginationDisabled = false;
    this.searchService.emitSearch('clearTheSearch');
  }

  public onPaginationChange(page: number): void {
    this.postsDataService.allPosts(page).subscribe(
      (posts: Post[]) => {
        this.data = posts;
        this.paginationPage = page;

        this.through = Math.min((page * this.pageSize), this.paginationCollectionSize);
        this.viewing = Math.min(this.pageSize, this.through - ((page * this.pageSize) - (this.pageSize - 1))) + 1;

        const url = this.router.createUrlTree([], {relativeTo: this.activatedRoute, queryParams: {page}})
          .toString();

        this.location.go(url);
      },
      (error) => this.notificator.error('Something went wrong!')
    );
  }
}
