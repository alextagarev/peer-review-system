import { Component } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-ngbd-carousel-basic', 
  templateUrl: './carousel-basic.component.html', 
  styleUrls: ['./carousel-basic.component.css'],
  providers: [NgbCarouselConfig]
})
export class NgbdCarouselBasicComponent {
  public imageOne = '../../assets/media/1.jpg';
  public imageTwo = '../../../assets/media/2.jpg';
  public imageThree = '../../../assets/media/3.jpg';

  constructor(config: NgbCarouselConfig) {
 
    config.interval = 3000;
    config.pauseOnHover = false;
    config.keyboard = true;
    config.showNavigationArrows = false;
    config.showNavigationIndicators = false;
  }

}