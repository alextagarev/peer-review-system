import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-breadcrumbs-search',
  templateUrl: './breadcrumbs-search.component.html',
  styleUrls: ['./breadcrumbs-search.component.css']
})
export class BreadcrumbsSearchComponent implements OnInit {
  @Input() public showCreatePostButton: boolean;
  @Input() public showCreateTeamButton: boolean;
  @Input() public showSearch: boolean;

  constructor() { }

  ngOnInit() {
  }

}
