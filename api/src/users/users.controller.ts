import { UsersService } from '../core/services';
import { AuthGuard } from '@nestjs/passport';
import { UserDec } from '../common/decorators';
import { WorkItemVoteDTO } from '../models';
import { Controller, Get, UseGuards, Post, Body, ValidationPipe, Param, Put } from '@nestjs/common';
import { ShowUserDTO } from '../models/show-user-dto';

@Controller('users')
export class UsersController {

    public constructor(
        private readonly usersService: UsersService,
    ) { }


    @Get('teams/reviews/:teamId')
    @UseGuards(AuthGuard())
    async getTeamMembersReviewRequests(
        @Param('teamId') teamId: string,
    ) {
        return await this.usersService.getTeamMembersReviewRequests(teamId);
    }

    @Get('reviews')
    @UseGuards(AuthGuard())
    async getUserReviewRequests(
        @UserDec() user: any,
    ) {
        return await this.usersService.getSingleUserReviewRequests(user);
    }

    @Get('reviews/all')
    @UseGuards(AuthGuard())
    async getAllReviewRequests(
        @UserDec() user: any,
    ) {
        return await this.usersService.getAllReviewRequest(user);
    }

    @Get('reviews/:workItemId')
    @UseGuards(AuthGuard())
    async getReviewRequest(
        @UserDec() user: any,
        @Param('workItemId') workItemId: any,
    ) {
        return await this.usersService.getSingleReviewRequest(workItemId);
    }

    @Post('reviews')
    @UseGuards(AuthGuard())
    async createReviewRequest(
        @UserDec() user: any,
        @Body(new ValidationPipe({ transform: true, whitelist: true })) body: any,
    ) {
        return await this.usersService.createReviewRequest(user, body);
    }

    @Put('workItems/:vote/:workItemId')
    @UseGuards(AuthGuard())
    async assignVote(
        @UserDec() user: any,
        @Param('vote') vote: WorkItemVoteDTO,
        @Param('workItemId') workItemId: string,
    ) {
        return await this.usersService.createVote(user, vote, workItemId);
    }

    @Post('workItems/:workItemId/:vote')
    @UseGuards(AuthGuard())
    async createVote(
        @UserDec() user: any,
        @Param('vote') vote: WorkItemVoteDTO,
        @Param('workItemId') workItemId: string,
    ) {
        return await this.usersService.createVote(user, vote, workItemId);
    }

    @Post(':workItemId')
    @UseGuards(AuthGuard())
    async createComment(
        @UserDec() user: any,
        @Body() body: any,
        @Param('workItemId') workItemId: string,
    ) {
        return await this.usersService.createComment(user, body, workItemId);
    }

    @Get('teams')
    @UseGuards(AuthGuard())
    async getUserTeams(
        @UserDec() user: any,
    ) {
        return await this.usersService.getSingleUserTeams(user);
    }

    @Get()
    async findAll(): Promise<ShowUserDTO[]> {
        return this.usersService.findAllUsers();
    }

    @Get(':userId')
    async findSingleUser(
      @Param('userId') userId: string,
    ): Promise<ShowUserDTO> {
      return this.usersService.findSingleUser(userId);
    }

}
