import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User, Team, Members, WorkItem, ReviewRequest, ReviewVotes, Comment } from '../../data/entities';
import { Repository } from 'typeorm';
import { WorkItemVoteDTO } from '../../models';
import { WorkItemStatus } from '../../common/enums/work-tem-status';
import { ReviewVoteStatus } from '../../common/enums/review-vote-status';

import { Reviewers } from '../../data/entities/reviewers';
import { config } from '../../common/config';
import { ShowUserDTO } from '../../models/show-user-dto';
import { MailerService } from '@nest-modules/mailer';

@Injectable()
export class UsersService {

    public constructor(private readonly mailerService: MailerService,
        @InjectRepository(Team) private readonly teamRepo: Repository<Team>,
        @InjectRepository(Members) private readonly membersRepo: Repository<Members>,
        @InjectRepository(User) private readonly userRepo: Repository<User>,
        @InjectRepository(WorkItem) private readonly workItemRepo: Repository<WorkItem>,
        @InjectRepository(ReviewRequest) private readonly reviewRequestRepo: Repository<ReviewRequest>,
        @InjectRepository(ReviewVotes) private readonly reviewVotesRepo: Repository<ReviewVotes>,
        @InjectRepository(Reviewers) private readonly reviewersRepo: Repository<Reviewers>,
        @InjectRepository(Comment) private readonly commentRepo: Repository<Comment>,
    ) { }

    private async convertToShowUserDTO(user: User): Promise<ShowUserDTO> {

        const convertedUser: ShowUserDTO = {
          id: user.id,
          name: user.name,
          email: user.email,
          createdOn: user.createdOn,
        };

        return convertedUser;
      }

    private async convertToShowUserDTOArray(users: User[]): Promise<ShowUserDTO[]> {
        return Promise.all(users.map(async (entity: User) => this.convertToShowUserDTO(entity)));
    }

    async findAllUsers(): Promise<ShowUserDTO[]> {
        const userEntities:User[] = await this.userRepo.find({
          where: {

          },
        });
        return this.convertToShowUserDTOArray(userEntities);
    }

    async findSingleUser(userId: string): Promise<ShowUserDTO>{
        const userEntity: User = await this.userRepo.findOne({
          where: {
            id: userId,

          },
        });
        if (!userEntity) {
          throw new BadRequestException('User with such ID does not exist.');
        }
        return this.convertToShowUserDTO(userEntity);
      }

      async getAllReviewRequest(workItemId: string) {
        return await this.workItemRepo.find();
    }

    async getAllUsers() {
        return await this.userRepo.find();
    }

    async getAllWorkItems() {
        return await this.workItemRepo.find({
            relations: ['assignee'],
        });
    }

    async getSingleUserTeams(user: User) {
        const foundUser = await this.userRepo.findOne({ id: user.id });

        const foundTeams: any = await this.membersRepo.find({
            relations: ['team'],
            where: {
                member: foundUser,
                isInTeam: true,
            },
        });

        return foundTeams.map(x => x.__team__);
    }

    async getSingleReviewRequest(workItemId: string) {
        const foundWorkItem: any = await this.workItemRepo.findOne({
            where: {
                id: workItemId,
            },
        });

        return foundWorkItem;
    }

    async getWorkItemComments(workItemId: string) {
        const foundWorkItem = await this.workItemRepo.findOne({ id: workItemId });
        return await this.commentRepo.find({
            where: {
                workItem: foundWorkItem,
            },
            relations: ['author'],
        });
    }

    async getSingleUserReviewRequests(user: User) {
        const assignee = await this.userRepo.findOne({ id: user.id });
        return await assignee.workItems;
    }

    async getTeamMembersReviewRequests(teamId: string) {
        const foundTeam: any = await this.teamRepo.findOne({
            where: {
                id: teamId,
            },
            relations: ['workItems'],
        });

        return foundTeam;
    }

    async createReviewers(usersArr: User[], workItemId: string) {
        const foundWorkItem = await this.workItemRepo.findOne({ id: workItemId });

        usersArr.map(async x => {
            const reviewingWorkItem = new Reviewers();
            reviewingWorkItem.reviewers = Promise.resolve(x);
            reviewingWorkItem.workItem = Promise.resolve(foundWorkItem);

            await this.reviewersRepo.save(reviewingWorkItem);
        });
    }

    async createReviewRequest(user: User, body: any) {
        const reviewAuthor = await this.userRepo.findOne({ id: user.id });
        const bodyReviwerersArray = body.reviewers.split(',').map(x => x.trim());
        const findReviewers = bodyReviwerersArray.map(async id => await this.userRepo.findOne({ id }));
        const allReviewers = await Promise.all(findReviewers);
        const sortedReviewers = allReviewers.filter(x => !!x);
        const userTeams = await this.teamRepo.findOne({
            where: {
                id: body.teams,
            },
        });

        const workItem = new WorkItem();
        workItem.assignee = Promise.resolve(reviewAuthor);
        workItem.reviewers = Promise.resolve(sortedReviewers);
        workItem.title = body.title;
        workItem.description = body.description;
        workItem.tags = body.comments;
        workItem.status = WorkItemStatus.Pending;
        workItem.teams = Promise.resolve([userTeams]);
  
        const savedWorkItem = await this.workItemRepo.save(workItem);
        const reviewRequest = new ReviewRequest();
        reviewRequest.author = Promise.resolve(reviewAuthor);
        reviewRequest.workItem = Promise.resolve(savedWorkItem);
        
        this
        .mailerService
        .sendMail({
          to: `${reviewAuthor.email},${body.reviewers}`,
          from: 'codenetix.telerik@gmail.com',
          subject: 'CODENETIX - New Review Request created.',
          text: 'New Review Request created.',
          html: '<h4>Hello,<br><br>You have been assigned as a reviewer to a new Review Request.<br><br>For more info visit: www.codenetix.com<br><br>Best regards',
        })
        .then(() => {})
        .catch(() => {});

        return await this.reviewRequestRepo.save(reviewRequest);
 
    }

    async createVote(user: User, vote: WorkItemVoteDTO, workItemId: string): Promise<any> {
        const foundWorkItem = await this.workItemRepo.findOne({
            relations: ['assignee'],
            where: {
                id: workItemId,
            },
        });

        const alreadyVoted = await this.reviewVotesRepo.findOne({
            relations: ['author', 'workItem'],
            where: {
                author: user,
                workItem: foundWorkItem,
            },
        });

        if (alreadyVoted) {
            throw new BadRequestException('Already Voted');
        }

        const voteStatus = vote.state;

        const newVote = new ReviewVotes();
        newVote.author = Promise.resolve(user);
        newVote.workItem = Promise.resolve(foundWorkItem);
        newVote.state = ReviewVoteStatus[ReviewVoteStatus[+voteStatus]];

        await this.reviewVotesRepo.create(newVote);
        await this.reviewVotesRepo.save(newVote);

        const workItemVotes = await this.reviewVotesRepo.find({
            relations: ['workItem'],
            where: {
                workItem: foundWorkItem,
            },
        });


        const voteValue: any = vote;

        if (voteValue === '2') {
            foundWorkItem.status = WorkItemStatus.Rejected;
            await this.workItemRepo.save(foundWorkItem);
            return newVote;
        } else {
            let voteCounter = 0;
            workItemVotes.forEach(x => {
                if (x.state === 1) {
                    voteCounter++;
                }
            });

            if (voteCounter >= config.workItemVoteCounter) {
                foundWorkItem.status = WorkItemStatus.Accepted;
                await this.workItemRepo.save(foundWorkItem);

                const reviewersTable = await this.reviewersRepo.find(
                    {
                        where: {
                            workItem: foundWorkItem,
                        },
                        relations: ['reviewers'],
                    },
                );

                const reviewers = await reviewersTable.map((x: any) => x.__reviewers__);

                this
                .mailerService
                .sendMail({
                to: `${user.email}`,
                from: 'codenetix.telerik@gmail.com',
                subject: 'CODENETIX - Review Request status changed.',
                text: 'Review Request status changed.',
                html: `<h4>Hello ${user.name}, workitem with title ${foundWorkItem.title} status was changed to ${foundWorkItem.status};.<br><br>For more info visit: www.codenetix.com<br><br>Best regards</h4>`,
                })
                .then(() => {})
                .catch(() => {});


                return newVote;
            }
        }
    }

    async createComment(user: User, body: any, workItemId: string) {
        const foundUser = await this.userRepo.findOne({ id: user.id });
        const foundWorkItem = await this.workItemRepo.findOne({ id: workItemId });

        const newComment = new Comment();
        newComment.author = Promise.resolve(foundUser);
        newComment.title = body.title; 
        newComment.description = body.description;
        newComment.workItem = Promise.resolve(foundWorkItem);

        const reviewersTable = await this.reviewersRepo.find(
            {
                where: {
                    workItem: foundWorkItem,
                },
                relations: ['reviewers'],
            },
        );
        const reviewers = await reviewersTable.map((x: any) => x.__reviewers__);

        return await this.commentRepo.save(newComment);
    }

}