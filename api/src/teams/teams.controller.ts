import { Controller, UseGuards, Get, Param, Post, Body, ValidationPipe, Put, Delete } from '@nestjs/common';
import { TeamService } from '../core/services';
import { AuthGuard } from '@nestjs/passport';
import { FindTeamGuard, FindUserGuard} from '../common/guards';
import { UserDec } from '../common/decorators';
import { CreateTeamDTO } from '../models';
import { Team, User } from '../data/entities';

@Controller('teams')
export class TeamsController {

    constructor(
        private readonly teamService: TeamService,
    ) { }

    @Get()
    @UseGuards(AuthGuard())
    async getAllTeams(): Promise<Team[]> {
        return await this.teamService.getAllTeams();
    }

    @Get('workItems')
    @UseGuards(AuthGuard())
    async getAllTeamWorkItems(
    ): Promise<any> {
        return await this.teamService.getAllTeamWorkItems();
    }

    @Get(':teamId')
    @UseGuards(AuthGuard())
    async getTeamDetails(
        @Param('teamId') teamId: string,
    ): Promise<Team> {
        return await this.teamService.getTeamDetails(teamId);
    }

    @Get('members/:teamId')
    @UseGuards(AuthGuard(), FindTeamGuard)
    async getTeamMembers(
        @Param('teamId') teamId: string,
    ): Promise<User[]> {
        return await this.teamService.getTeamMembers(teamId);
    }

    @Post()
    @UseGuards(AuthGuard())
    async createNewTeam(
        @UserDec() user: any,
        @Body(new ValidationPipe({ transform: true, whitelist: true })) team: CreateTeamDTO): Promise<Team> {
        return await this.teamService.createNewTeam(user, team);
    }

    @Put(':teamId/:userId')
    @UseGuards(AuthGuard(), FindTeamGuard, FindUserGuard)
    async addNewTeamMember(
        @UserDec() user: any,
        @Param('teamId') teamId: string,
        @Param('userId') userId: string,
    ): Promise<any> {
        return await this.teamService.addNewTeamMember(user, teamId, userId);
    }

}
