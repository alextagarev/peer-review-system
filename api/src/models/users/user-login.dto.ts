import { Length, IsEmail, IsString, Matches } from 'class-validator';

export class UserLoginDTO {
    @IsString()
    @IsEmail()
    email: string;

    @IsString()
    @Length(4, 20, {
        message: 'Password should be between 4 and 20 characters.',
    })
    // @Matches(/(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{4,}/)
    password: string;
}
