import { Length, IsEmail, IsString, Matches } from "class-validator";

export class RegisterUserDTO {
    @IsString()
    @Length(4, 20, {
        message: 'Username should be between 4 and 20 characters.',
    })
    name: string;

    @IsString()
    // @Matches(/(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{4,}/)
    @Length(4, 20, {
        message: 'Password should be between 4 and 20 characters.',
    })
    password: string;

    @IsString()
    @IsEmail()
    email: string;
}
